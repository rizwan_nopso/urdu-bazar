import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams ,AlertController} from 'ionic-angular';
import { LoginPage } from '../login/login';
import { AuthProvider } from '../../providers/auth/auth';

/**
 * Generated class for the RegisterPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-register',
  templateUrl: 'register.html',
})
export class RegisterPage {

  username:string = '';
  password:string = '';
  email:string = '';

  constructor(public navCtrl: NavController, public navParams: NavParams,public authService:AuthProvider,public alertCtrl:AlertController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RegisterPage');
  }

  login(){
    this.navCtrl.push(LoginPage);
  }

  errorFunc(message){
    let alert = this.alertCtrl.create({
      title:'Warning !',
      subTitle:message,
      buttons:['ok']
    });
    alert.present();
  }

  myRegister(){
    if(this.email.trim() && this.username.trim() && this.password.trim()){
      if(this.password.trim()===''){
        this.errorFunc("please enter your password");
      }else{
        let credentials = {
          email:this.email,
          username:this.username,
          password:this.password
        };
        this.authService.creatAccount(credentials).then((result)=>{
          console.log(result);
          this.navCtrl.setRoot(LoginPage);
        },(err) => {
          console.log(err);
          this.errorFunc("wrong credentials ! try again ");
          console.log("credentials: "+JSON.stringify(credentials));
        });
      }
    }else{
      this.errorFunc("Please enter a valid password ! for ex:(12345)");
    }
    console.log(console.log(this.username.trim()+"  "+this.password.trim()));
  }

}
