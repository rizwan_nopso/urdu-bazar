import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { Storage } from '@ionic/storage';
import { Http,Headers, RequestOptions } from '@angular/http';
import { apiKey } from '../../app/apiurls/serverurls';

@Injectable()
export class AuthProvider {
  BaseURL = 'https://api.myjson.com/bins/ywpjm'
  headers : Headers
  options: RequestOptions
  
  token:any;

  constructor(public http: Http,public storage:Storage) {
    this.setUpHeaderAndOptions()
    console.log('Hello AuthProvider Provider');
  }

  creatAccount(details){
    return new Promise((resolve,reject) => {
      let headers = new Headers();
      headers.append('Content-Type','application/json');
      this.http.post(apiKey+'register', JSON.stringify(details),{headers:headers})
      .subscribe(res => {
        let data = res.json();
        // this.token = data.token;
        // this.storage.set('token',data.token);
        resolve(data);
      },(err) => {
        reject(err);
      });
    });
  }

  // login
  login(credentials){
    return new Promise((resolve,reject) => {
      let headers = new Headers();
      // headers.append('Content-Type','application/json')
      headers.append('Access-Control-Allow-Origin','*');
      headers.append('Access-Control-Allow-Methods','POST,GET,OPTIONS,PUT');
      headers.append('Accept','application/json');
      headers.append('content-type','application/json');

      this.http.post(apiKey+'/api/auth/create/',JSON.stringify(credentials),{headers:headers})
      .subscribe(res => {
        let data = res.json();
        this.token = data.token;
        this.storage.set('token',data.token);
        //localStorage.setItem('isLoggedIn','true');
        resolve(data);
      }, (err) => {
        reject(err);
      });
    });
  }

  // checkAUthentications
  checkAuthentication(){
    return new Promise((resolve,reject) => {
      this.storage.get('token').then((value) => {
        this.token = value;
        resolve(this.token);
      })
    });
  }

  logout(){
    this.storage.set('token','');
  }


  getCategory() {
    this.BaseURL  = "http://127.0.0.1:8000/api/categories";
    return this.http.get(this.BaseURL , this.options)
      .map(Response => Response.json());
  }
  getSubCategory(id) {
    this.BaseURL  = "http://127.0.0.1:8000/api/subcategory/"+id;
    return this.http.get(this.BaseURL , this.options)
      .map(Response => Response.json());
  }
  setUpHeaderAndOptions() {
    this.headers = new Headers();
    this.headers.append('Accept', 'application/json');
    this.headers.append('Content-Type', 'application/json');
    this.headers.append('Access-Control-Allow-Origin', '*');
    this.headers.append('Access-Control-Allow-Methods', 'GET,POST')
    this.headers.append('Access-Control-Allow-Headers', 'Authorization, Content-Type')

    this.options = new RequestOptions({ headers: this.headers });
  }  
}
