import { AuthProvider } from './../../providers/auth/auth';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';


import { ProductPage } from '../product/product';
/**
 * Generated class for the CategoryPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-category',
  templateUrl: 'category.html',
})
export class CategoryPage {
  id:any

  constructor(public navCtrl: NavController, public navParams: NavParams,public authService:AuthProvider) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CategoryPage');
    this.id = this.navParams.get('Id')
    this.getSubCategory(this.id);
  }

  product(){
    this.navCtrl.push(ProductPage);
  }

  getSubCategory(id){
    this.authService.getSubCategory(id).subscribe(res => {
      console.log(res);
    });
  }

}
