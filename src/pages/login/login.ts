import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthProvider } from '../../providers/auth/auth';
import { RegisterPage } from '../register/register';
import { HomePage } from '../home/home';


@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

  public loginForm: FormGroup;
   public submitAttempt: boolean = false;
   public loginError: boolean = false;
   public loginMessage;
   public shipping;
   public fireAuth: any;

   email:string = '';
   password:string = '';
   

  constructor(public navCtrl: NavController, public navParams: NavParams, public formBuilder: FormBuilder,public alertCtrl:AlertController,private authService:AuthProvider) {

      this.loginForm = formBuilder.group({
          email: ['',Validators.required],
          password: ['', Validators.compose([Validators.minLength(8), 
          Validators.pattern('[a-zA-Z0-9 ]*'), Validators.required])],    
        });
        if(this.navParams.get('member') == 'memberLogin'){
          this.shipping = true;
        }
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
  }
  register(){
    this.navCtrl.push(RegisterPage);
  }

    errorFunc(message){
      let alert= this.alertCtrl.create({
        title:'Warining',
        subTitle:message,
        buttons:['ok']
      });

      alert.present();
    }

    myLogin(){
      if(this.email.trim() !==''){
        console.log(this.email.trim());
        if(this.password.trim() === ''){
          this.errorFunc("please enter your password");
        }else{
          let credentials = {
            email:this.email,
            password:this.password
          };
          
          this.authService.login(credentials).then((result) => {
            console.log(result);
            this.navCtrl.setRoot(HomePage);
          },(err) => {
            console.log(err);
            this.errorFunc("Wrong credentials ! try again");
            console.log("credentials: "+JSON.stringify(credentials));
          });

        }
      }else{
        this.errorFunc("please enter a valid password ! for ex:(12345)");
      }

      console.log(console.log(this.email.trim()+"  "+this.password.trim()));

    }
    myLogOut(){
      this.authService.logout();
    }

  resetPassword() {
    const prompt = this.alertCtrl.create({
      title: 'Login',
      message: "Enter a name for this new album you're so keen on adding",
      inputs: [
        {
          name: 'title',
          placeholder: 'Title'
        },
      ],
      buttons: [
        {
          text: 'Cancel',
          handler: data => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Save',
          handler: data => {
            console.log('Saved clicked');
          }
        }
      ]
    });
    prompt.present();
  }

}
