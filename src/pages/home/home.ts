import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { AuthProvider } from '../../providers/auth/auth';

import { CategoryPage } from '../category/category';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  mydata:any;
  cate_id:any;

  constructor(public navCtrl: NavController,public authService:AuthProvider) {
    this.authService.getCategory().subscribe(res =>{
      console.log(res.data);
     this.mydata = res.data;
      
    })
    
  }

  category(id){
    this.cate_id = id;
    this.navCtrl.push(CategoryPage,{Id:this.cate_id});
  }

}
