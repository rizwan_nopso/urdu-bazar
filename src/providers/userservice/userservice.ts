import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

import 'rxjs/add/operator/map';

/*
  Generated class for the UserserviceProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class UserserviceProvider {

  public fireAuth: any;
	public userProfile: any;
  public firstname: string;
  public lastname: string;
  public email: string;

  constructor(public http: HttpClient) {
    console.log('Hello UserserviceProvider Provider');
  }

  

}
